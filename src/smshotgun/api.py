import tank
   
def getShotgun():
    return tank.platform.current_engine().shotgun
    
def getTank():
    return tank.platform.current_engine().tank

def getTankFramPath(filePath):
    return tank.tank_from_path(filePath)
    
def getTemplateFromPath(filePath):
    _tank = getTankFramPath(filePath)
    return  _tank.template_from_path(filePath)
    
def getTemplateFieldsFromPath(filePath):
    _tank = getTankFramPath(filePath)
    template = _tank.template_from_path(filePath)
    return template.get_fields(filePath)
    
def getPublishTypeFromPath(filePath):
    _sg = getShotgun()
    entity = getContextFromPath(filePath).entity
    version = getTemplateFieldsFromPath(filePath)['version']
    filters = [
                ["entity", "is", entity],
                ['version_number', 'is', version]
              ]
    fields = ["version_number", "path", 'published_file_type']
    dataList = _sg.find('PublishedFile', filters, fields)
    if not dataList:
        return
    for data in dataList:
        if data['path']['local_path'].replace('\\', '/') == filePath.replace('\\', '/'):
            return data['published_file_type']['name']
            
def getCurrentContext():
    """
    get rebuild context of the current task ,with assetType and geoVariant data
    return: e.g.
    {
        project:{'type': 'Project', 'name': 'Thingdom', 'id': 87}
        entity:{'type': 'Asset', 'name': 'boCai', 'id': 4997, 'assetType':'character'}
        step:{'type': 'Step', 'name': 'lay', 'id': 13}
        task:{'type': 'Task', 'name': 'lay', 'id': 26757, 'geoVariant':'std'}
    }
    """
    context = tank.platform.current_engine().context
    return context

def getContextFromPath(path):
    """
    return the origin sg context from publish file path
    """
    _tank = tank.tank_from_path(path)
    return _tank.context_from_path(path)
    
def getAssetType(entity):
    """
    return the sg_asset_type of asset
    """
    sg = tank.platform.current_engine().shotgun
    assetType = sg.find('Asset', [['id', 'is', entity['id']]], ['sg_asset_type'])[0]['sg_asset_type']
    return assetType

def getAssetEntity(data):
    """
    get Asset entity by assetName and assetType 
    return: sg asset entitys
    """
    sg = getShotgun()
    assetName = data['asset']
    assetType = data['type']
    filters = [['code', 'is', assetName], 
                ['sg_asset_type', 'is', assetType],
                ]
    entity = sg.find('Asset', filters)
    return entity

def getStepTask(entity, step, extraFilters=None):
    """
    get all task entitys of step 
    return: sg taks entitys
    """    
    sg = getShotgun()
    filters = [
                ["entity", "is", entity],
                ['step', "name_is", step]
                ]
    if extraFilters and isinstance(extraFilters, list):
        filters.extend(extraFilters)
    tasks = sg.find('Task', filters, ['content'])
    return tasks
        
def getPublishData(entity, publishType, extraFilters=None):
    """
    get publish data by publish type and entity
    return: sg publish data
    """
    sg = getShotgun()
    filters = [
                ["entity", "is", entity],
                ["published_file_type", "name_is", publishType],
              ]
    if extraFilters and isinstance(extraFilters, list):
        filters.extend(extraFilters)
    fields = ["version_number", "path", 'name']
    sg_data = sg.find('PublishedFile', filters, fields)
    return sg_data
    
def getTaskPublishData(entity, publishType, taskName):
    """
    get task publish data
    """
    extraFilters = [['task', 'name_is', taskName]]
    sgPublishData = getPublishData(entity, publishType, extraFilters)
    return sgPublishData
    
def getPublishVersionData(publishData):
    resultData = {}
    for data in publishData:
        resultData[data['version_number']] = data['path']['local_path']
    return resultData
    
def setShotgunField(entity, data):
    sg = getShotgun()
    sg.update(entity["type"], entity["id"], data)
    